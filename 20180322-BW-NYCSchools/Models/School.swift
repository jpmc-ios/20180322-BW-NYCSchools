//
//  School.swift
//  20180322-BW-NYCSchools
//
//  Created by Brian Weissberg on 3/22/18.
//  Copyright © 2018 Brian Weissberg. All rights reserved.
//

import Foundation

struct School: Decodable {
    
    //
    // MARK: - Variables
    //
    
    let name: String
    let city: String
    let totalStudents: String
    let dbn: String
    
    //
    // MARK: - Coding Keys
    //
    
    enum CodingKeys: String, CodingKey {
        case name = "school_name"
        case city
        case totalStudents = "total_students"
        case dbn
    }
    
    //
    // MARK: - Initalizer
    //
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let name = try values.decode(String.self, forKey: .name)
        let city = try values.decode(String.self, forKey: .city)
        let totalStudents = try values.decode(String.self, forKey: .totalStudents)
        let dbn = try values.decode(String.self, forKey: .dbn)
        
        self.name = name
        self.city = city
        self.totalStudents = totalStudents
        self.dbn = dbn
    }
}
