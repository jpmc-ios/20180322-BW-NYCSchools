//
//  SchoolController.swift
//  20180322-BW-NYCSchools
//
//  Created by Brian Weissberg on 3/22/18.
//  Copyright © 2018 Brian Weissberg. All rights reserved.
//

import Foundation

class SchoolController {
    
    //
    // MARK: - Shared
    //
    
    static let shared = SchoolController()
    
    //
    // MARK: - Variables
    //
    
    var schools = [School]()
    
    //
    // MARK: - Methods
    //
    
    func fetchSchools(completion: @escaping (_ school: [School]) -> Void) {
        
        // URL: I like to put #file and #function into my guard statements so that if it returns early, I know what is wrong with my functions and can debug easier.
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/97mf-9njv.json") else {
            print("There was an error with with URL. File: \(#file). Function: \(#function)")
            completion([])
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) -> Void in
            
            // Handle errors. I like to have #file and #function here in dataTasks for debugging purposes
            if let error = error {
                print("There was an a error while fetching data: \(error.localizedDescription). File: \(#file). Function: \(#function)")
                completion([])
            }
            
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                print("There was an invalid HTTP response. File: \(#file). Function: \(#function)")
                completion([])
                return
            }
            
            guard let data = data else {
                print("No data was returned. File: \(#file). Function: \(#function)")
                completion([])
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let schoolsArray = try decoder.decode([School].self, from: data)
                self.schools = schoolsArray
                completion(self.schools)
            } catch {
                print("There was an error thrown in the do/catch statement. File: \(#file). Fuction: \(#function). Error: \(error.localizedDescription)")
                completion([])
            }
        }
        dataTask.resume()
    }
}
