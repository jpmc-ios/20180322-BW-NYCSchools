//
//  Keys.swift
//  20180322-BW-NYCSchools
//
//  Created by Brian Weissberg on 3/22/18.
//  Copyright © 2018 Brian Weissberg. All rights reserved.
//

import Foundation
import UIKit

class Keys {
    static let schoolCell = "schoolCell"
    static let toSchoolDetailsVC = "toSchoolDetailsVC"
    static let salmon = UIColor(red: 253.0/255.0, green: 127.0/255.0, blue: 124.0/255.0, alpha: 1.0)
    static let customBlue = UIColor(red: 148.0/255.0, green: 195.0/255.0, blue: 245.0/255.0, alpha: 1.0)
    static let detailTextColor = UIColor.white
    static let mainTextColor = UIColor.darkText
}

