//
//  SchoolDetailsViewController.swift
//  20180322-BW-NYCSchools
//
//  Created by Brian Weissberg on 3/22/18.
//  Copyright © 2018 Brian Weissberg. All rights reserved.
//

import Foundation
import UIKit

class SchoolDetailsViewController: UIViewController {
    
    //
    // MARK: - View Lifecycle
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        formatTextAndBackgroundColors()
        insufficientInformationView.isHidden = true
        searchForSchoolDetailsUsingDBN(dbn: dbn)
        setUpLabels()
    }
    
    //
    // MARK: - Outlets
    //
    
    @IBOutlet weak var readingLabel: UILabel!
    @IBOutlet weak var writingLabel: UILabel!
    @IBOutlet weak var mathLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var AveragesTitleLabel: UILabel!
    @IBOutlet weak var averageWritingLabel: UILabel!
    @IBOutlet weak var averageMathLabel: UILabel!
    @IBOutlet weak var averageReadingLabel: UILabel!
    @IBOutlet weak var insufficientInformationView: UIView!
    
    //
    // MARK: - Variables
    //
    
    var schoolDetails: SchoolDetails?
    var dbn = String()
    
    //
    // MARK: - Methods
    //
    
    /// This function takes a schools DBN and uses that number to lookup schoolDetail information.
    func searchForSchoolDetailsUsingDBN(dbn: String) {
        for i in SchoolDetailsController.shared.schoolDetailsArray {
            if i.dbn == dbn && i.mathScore != "" && i.readingScore != "" && i.writingScore != "" {
                self.schoolDetails = i
            }
        }
    }
    
    /// This function formats each schools name to have all letters capitalized
    func returnCapitalizedName(name: String) -> String? {
        guard let schoolName = schoolDetails?.name else { return nil }
        let formattedName = schoolName.lowercased().capitalized
        return formattedName
    }
    
    /// The function sets up all labels on the screen
    func setUpLabels() {
        
        guard let schoolDetails = schoolDetails else {
            insufficientInformationView.isHidden = false
            return }
        
        let overallWritingAverage = returnAverage(ofCurrentSchool: schoolDetails.writingScore, againstOverallAverage: SchoolDetailsController.shared.writingAverage, typeOfScore: "Writing")
        let overallMathAverage = returnAverage(ofCurrentSchool: schoolDetails.mathScore, againstOverallAverage: SchoolDetailsController.shared.mathAverage, typeOfScore: "Math")
        let overallReadingAverage = returnAverage(ofCurrentSchool: schoolDetails.readingScore, againstOverallAverage: SchoolDetailsController.shared.readingAverage, typeOfScore: "Reading")
        
        DispatchQueue.main.async {
            self.readingLabel.text = "Reading Average SAT Score: \(schoolDetails.readingScore)"
            self.writingLabel.text = "Writing Average SAT Score: \(schoolDetails.writingScore)"
            self.mathLabel.text = "Math Average SAT Score: \(schoolDetails.mathScore)"
            self.nameLabel.text = self.returnCapitalizedName(name: schoolDetails.name)
            self.averageWritingLabel.text = "\(overallWritingAverage)"
            self.averageMathLabel.text = "\(overallMathAverage)"
            self.averageReadingLabel.text = "\(overallReadingAverage)"
        }
    }
    
    /// This function will determine if a school is above, at or below average for scores passed in
    func returnAverage(ofCurrentSchool a: String, againstOverallAverage overallAverage: Int, typeOfScore type: String) -> String {
        
        guard let schoolAverage = Int(a) else { return String() }
        
        if schoolAverage < overallAverage {
            return "\(type): \(a) is below the average of \(overallAverage)"
        } else if schoolAverage > overallAverage {
            return "\(type): \(a) is above the average of \(overallAverage)"
        } else if schoolAverage == overallAverage {
            return "\(type): \(a) is equal to the average of \(overallAverage)"
        } else {
            return "Insufficient Data"
        }
    }
    
    func formatTextAndBackgroundColors() {
        
        // Format Background
        self.view.backgroundColor = Keys.salmon
        
        // Format the detail Text Labels
        self.readingLabel.textColor = Keys.detailTextColor
        self.writingLabel.textColor = Keys.detailTextColor
        self.mathLabel.textColor = Keys.detailTextColor
        self.averageReadingLabel.textColor = Keys.detailTextColor
        self.averageWritingLabel.textColor = Keys.detailTextColor
        self.averageMathLabel.textColor = Keys.detailTextColor
        
        // Format Title Text
        self.nameLabel.textColor = Keys.mainTextColor
        self.AveragesTitleLabel.textColor = Keys.mainTextColor
    }
}
