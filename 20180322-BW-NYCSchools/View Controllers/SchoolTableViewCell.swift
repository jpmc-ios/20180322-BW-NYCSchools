//
//  SchoolTableViewCell.swift
//  20180322-BW-NYCSchools
//
//  Created by Brian Weissberg on 3/22/18.
//  Copyright © 2018 Brian Weissberg. All rights reserved.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {

    //
    // MARK: - Variables
    //
    
    var school: School? {
        didSet {
            DispatchQueue.main.async {
                self.updateCell()
            }
        }
    }
    
    //
    // MARK: - Outlets
    //
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var totalStudentsLabel: UILabel!
    
    //
    // MARK: - Methods
    //
    
    func updateCell() {
        
        guard let school = school else { return }
        
        nameLabel.text = school.name
        cityLabel.text = school.city
        totalStudentsLabel.text = "Total Number Students: \(school.totalStudents)"
    }
}
