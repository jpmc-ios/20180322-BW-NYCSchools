//
//  SchoolDetailsController.swift
//  20180322-BW-NYCSchools
//
//  Created by Brian Weissberg on 3/22/18.
//  Copyright © 2018 Brian Weissberg. All rights reserved.
//

import Foundation

class SchoolDetailsController {
    
    //
    // MARK: - Shared
    //
    
    static let shared = SchoolDetailsController()
    
    //
    // MARK: - Variables
    //
    
    var schoolDetailsArray = [SchoolDetails]()
    var readingAverage = Int()
    var mathAverage = Int()
    var writingAverage = Int()
    
    //
    // MARK: - Methods
    //
    
    func fetchSchoolDetails(completion: @escaping (_ schoolDetails: [SchoolDetails]) -> Void) {
        
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/734v-jeq5.json") else {
            print("There was an error with the schoolDetails url. File: \(#file). Function: \(#function)")
            completion([])
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) -> Void in
            
            if let error = error {
                print("There was an error fetching data: File: \(#file). Function: \(#function). Error: \(error.localizedDescription)")
                completion([])
            }
            
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                print("Invalid response. File: \(#file). Function: \(#function)")
                completion ([])
                return
            }
            
            guard let data = data else {
                print("No data received. File: \(#file). Function: \(#function)")
                completion([])
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let schoolDetailsArray = try decoder.decode([SchoolDetails].self, from: data)
                self.schoolDetailsArray = schoolDetailsArray
                completion(self.schoolDetailsArray)
            } catch {
                print("There was an error in the do/catch statement. File: \(#file). Function: \(#function). Error: \(error.localizedDescription)")
                completion([])
            }
        }
        dataTask.resume()
    }
    
    /// This function calculates the average reading, writing and math SAT scores. 
    func calculateAverageScores() {
        
        var totalReading = 0
        var totalWriting = 0
        var totalMath = 0
        let numberOfSchools = schoolDetailsArray.count
        
        for i in schoolDetailsArray {
            totalReading += Int(i.readingScore) ?? 0
            totalWriting += Int(i.writingScore) ?? 0
            totalMath += Int(i.mathScore) ?? 0
        }
        
        self.readingAverage = totalReading/numberOfSchools
        self.writingAverage = totalWriting/numberOfSchools
        self.mathAverage = totalMath/numberOfSchools
    }
}
