//
//  SchoolTableViewController.swift
//  20180322-BW-NYCSchools
//
//  Created by Brian Weissberg on 3/22/18.
//  Copyright © 2018 Brian Weissberg. All rights reserved.
//

import UIKit

class SchoolTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //
    // MARK: - Variables
    //
    
    var schools = [School]()
    
    //
    // MARK: - Outlets
    //
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyStateView: UIView!
    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    
    //
    // MARK: - View Lifecycle
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        formatNavigationBar()
        showEmptyStateView()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        SchoolController.shared.fetchSchools { (schools) in
            DispatchQueue.main.async {
                self.schools = SchoolController.shared.schools
                self.tableView.reloadData()
                self.showTableView()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "New York Schools"
    }

    //
    // MARK: - TableView
    //

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Keys.schoolCell, for: indexPath) as? SchoolTableViewCell ?? SchoolTableViewCell()
        let school = schools[indexPath.row]
        cell.school = school
        formatTableView(andCell: cell)
        return cell
    }
    
    //
    // MARK: - Methods
    //
    
    func showEmptyStateView() {
        self.tableView.isHidden = true
        self.emptyStateView.isHidden = false
        self.activitySpinner.isHidden = false
        self.activitySpinner.startAnimating()
    }
    
    func showTableView() {
        self.tableView.isHidden = false
        self.activitySpinner.isHidden = true
        self.emptyStateView.isHidden = true
        self.activitySpinner.stopAnimating()
    }
    
    func formatTableView(andCell cell: SchoolTableViewCell) {
        self.tableView.separatorColor = Keys.customBlue
        self.tableView.backgroundColor = .gray
        cell.backgroundColor = Keys.salmon
        cell.backgroundView?.alpha = 0.5
        cell.cityLabel.textColor = .white
        cell.nameLabel.textColor = .black
        cell.totalStudentsLabel.textColor = .white
    }
    
    func formatNavigationBar() {
        self.navigationController?.navigationBar.barTintColor = Keys.customBlue
    }
    
    //
    // MARK: - Navigation
    //

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Keys.toSchoolDetailsVC {
            guard let destinationVC = segue.destination as? SchoolDetailsViewController,
                let indexPath = tableView.indexPathForSelectedRow else { return }
            
            let school = schools[indexPath.row]
            destinationVC.dbn = school.dbn
            self.navigationItem.title = ""
            self.tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}
